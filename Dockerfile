FROM fluent/fluentd:v1.13.0-debian-1.0
USER root
RUN fluent-gem install fluent-plugin-elasticsearch
USER fluent
