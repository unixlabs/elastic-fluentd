# build
```
docker build --tag registry.gitlab.com/unixlabs/elastic-fluentd:v1.13.0 .
docker push registry.gitlab.com/unixlabs/elastic-fluentd:v1.13.0
```
# pull
```
docker pull registry.gitlab.com/unixlabs/elastic-fluentd:v1.13.0
```
